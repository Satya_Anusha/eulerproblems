def prime(n):
  if n in [2,3,5,7]:
    return True
  if n%2==0:
    return False
  r=3
  while r*r<=n:
    if n%r==0:
      return False
    r=r+2
  return True
def check():
  dict1={}
  for i in range(2,100):
    l=[]
    sum1=0
    if prime(i):
      for j in range(2,i):
        if prime(j):
          sum1=sum1+j
          l.append(j)
          if i==sum1:
            dict1[i]=len(l)
  m=max(dict1.values())
  for i,j in dict1.items():
    if m==j:
      return i
check()    
          
