def prime(n):
  if n in [2,3,5,7]:
    return True
  if n%2==0:
    return False
  r=3
  while r*r<=n:
    if n%r==0:
      return False
    r=r+2
    return True
def factorize(n):
  while n>1 and n%2==0:
    yield 2
    n=n//2
  r=3
  while n>1:
    while(n%r==0):
      yield r
      n=n//r
    r=r+2
def distinct_prime_factor_count(n):
  return len(set(factorize(n)))
def numbers():
  for n in range(8,1000):
     if distinct_prime_factor_count(n)==distinct_prime_factor_count(n+1)==distinct_prime_factor_count(n+2)==3:
        return (n,n+1,n+2)
numbers()

      
